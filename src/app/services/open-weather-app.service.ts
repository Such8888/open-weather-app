import { Injectable } from '@angular/core';
import { HttpParams, HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class OpenWeatherAppService {
  baseIp = environment.baseIp;
  appId = 'bfdc1634c92d55fd4e7c97ba64d6fe49';

  constructor(private httpClient: HttpClient) {}

  getCurrentTemperature(cityname): Observable<any> {
    console.log(cityname);
    const params = new HttpParams();
    // .set('lat', lat)
    // .set('lon', lon)
    // // .set('exclude', exclude)
    // .set('appid', this.appId);

    return this.httpClient.get(
      `${this.baseIp}weather?q=${cityname}&appid=${this.appId}`
    );
  }

  getWeeklyTemperature(cityName, cnt): Observable<any> {
    return this.httpClient.get(
      `${this.baseIp}forecast/daily?q=${cityName}&cnt=${cnt}&appid=${this.appId}`
    );
  }
}
