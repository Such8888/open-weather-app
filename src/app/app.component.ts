import { ThisReceiver } from '@angular/compiler';
import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Country } from 'country-state-city';
import { OpenWeatherAppService } from './services/open-weather-app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'OpenWeatherMap';
  lon: number;
  lat: number;
  cnt: number;
  cityTemp: number;
  weather: string;
  cityTempInCelsius: number;

  cityList: any[] = [
    'Shanghai',
    'kathmandu',
    'Buenos Aires',
    'Mumbai',
    'Mexico City',
    'Karachi',
    'İstanbul',
    'Delhi',
    'Manila',
    'Moscow',
    'Dhaka',
    'Seoul',
    'São Paulo',
    'Lagos',
    'Jakarta',
    'Tokyo',
    'Zhumadian',
    'New York',
    'Taipei',
    'Kinshasa',
    'Lima',
    'Cairo',
    'London',
    'Beijing',
    'Tehrān',
    'Nanchong',
    'Bogotá',
    'Hong Kong',
    'Lahore',
    'Rio de Janeiro',
    'Baghdad',
    'Tai’an',
    'Bangkok',
    'Bangalore',
    'Yueyang',
    'Santiago',
    'Kaifeng',
    'Kolkata',
    'Toronto',
    'Yangon ',
    'Sydney',
    'Chennai',
    'Riyadh',
    'Wuhan',
    'Saint Petersburg',
    'Chongqing',
    'Chengdu',
    'Chittagong',
    'Alexandria',
    'Los Angeles',
    'Tianjin',
    'Melbourne',
    'Ahmadābād',
    'Pusan',
    'Abidjan',
    'Kano',
    'Hyderābād',
    'Puyang',
    'Ibadan',
    'Singapore',
    'Shenyang',
    'Hồ Chí Minh City',
    'Shiyan',
    'Cape Town',
    'Berlin',
    'Montréal',
    'Madrid',
    'Harbin',
    'Xi’an',
    'Pyongyang',
    'Lanzhou',
    'Guangzhou',
    'Casablanca',
    'Durban',
    'Nanjing',
    'Kabul',
    'Shenzhen',
    'Caracas',
    'Pune',
    'Sūrat',
    'Jeddah',
    'Kānpur',
    'Luanda ',
    'Addis Ababa',
    'Nairobi',
    'Taiyuan',
    'Salvador',
    'Jaipur',
    'Dar es Salaam',
    'Chicago',
    'Incheon',
    'Yunfu',
    'Al Başrah',
    'Ōsaka-shi',
    'Mogadishu',
    'Taegu',
    'Rome',
    'Changchun',
    'Kiev',
  ];
  city;

  constructor(private openWeatherService: OpenWeatherAppService) {}

  ngOnInIt() {}

  changeCity(event) {
    this.city = event.$ngOptionLabel;
    this.getCurrentTemperature();
  }

  getCurrentTemperature(): void {
    console.log(this.city);
    this.openWeatherService
      .getCurrentTemperature(this.city)
      .subscribe((response) => {
        if (response) {
          this.cityTemp = response.main.temp;
          this.cityTempInCelsius = this.cityTemp - 273.15;
          this.weather = response.weather[0].main;
        }
      });
  }

  weeklyCity;
  changeCityWeekly(event): void {
    this.weeklyCity = event.$ngOptionLabel;
    this.getWeeklyTemperature();
  }

  // getWeeklyTemperature(): void {
  //   this.openWeatherService
  //     .getWeeklyTemperature(44.34, 10.99, 7)
  //     .subscribe((response) => {
  //       if (response) {
  //       }
  //     });
  // }

  weathers;
  cityTempInCelsiuss;
  getWeeklyTemperature(): void {
    console.log(this.city);
    this.openWeatherService
      .getWeeklyTemperature(this.weeklyCity, 7)
      .subscribe((response) => {
        if (response) {
          this.cityTempInCelsiuss = response.weather[0];
          console.log(response);
        }
      });
  }

  // getCityList(): void {
  //   this.openWeatherService.getCityList().subscribe((res) => {
  //     for (let [i, a] of res.data.entries()) {
  //       for (let b of res.data[i].cities) {
  //         this.cityList.push(b);
  //       }
  //     }
  //     console.log(this.cityList, 'city');
  //   });
  // }
}
